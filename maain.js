class Film {
    constructor(id, title, director, trailerUrl, releaseYear, boxOffice) {
      this.id = id;
      this.title = title;
      this.director = director;
      this.trailerUrl = trailerUrl;
      this.releaseYear = releaseYear;
      this.boxOffice = boxOffice;
    }
  
    toString() {
      return `Фільм: ${this.title}\nРежисер: ${this.director}\nURL-трейлеру: ${this.trailerUrl}\nРік випуску: ${this.releaseYear}\nКасові збори: ${this.boxOffice};`
    }
  }
  
  // Створення об'єкта фільму
  var film = new Film(
    1,
    "Назва фільму",
    "Режисер",
    "URL-трейлеру",
    2023,
    "$100 млн"
  );
  
  
  
  class FilmCollection {
      films;
  
      constructor(films = []) {
          this.films = films;
  
          this.showFilms()
      }
  
      editFilmById(id, data) {
        const index = this.films.findIndex((film) => film.id === Number(id))
        
        if (index > -1) {
          this.films[index] = {...this.films[index], ...data}
        }
        alert("Успішно змінено")
  
        this.showFilms()
      }
  
      showFilms() {
        const parsedFilms = this.films.map((film) => `<li>
          <h2>Id: ${film.id}</h2>
          <h2>Title: ${film.title}</h2>
          <h2>Director: ${film.director}</h2>
          <h2><a href="${film.trailerUrl}">Film link</a></h2>
          <h2>Year: ${film.boxOffice}</h2>
          </li>`)
        
          const filmsList = document.getElementById('filmsList');
          filmsList.innerHTML = parsedFilms;
      }
  }
  
  const filmCollection1 = new FilmCollection([film]);
  
  const form = document.getElementById('form');
  
  form.addEventListener('submit', (e) => {
      e.preventDefault();
  
      const formData = new FormData(form);
      const data = {
        id: formData.get('id'),
        title: formData.get('title'),
        director: formData.get('director'),
        trailerUrl: formData.get('trailerUrl'),
        boxOffice: formData.get('boxOffice'),
      }
  
      filmCollection1.editFilmById(data.id, data);
  })